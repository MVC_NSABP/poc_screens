﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace NSABP_Model_BO_
{
    public class FormEntry_BO
    {
        public int protocolID { get; set; }
        public List<Sites> List_Sites { get; set; }
        public int ID { get; set; }
        [Display(Name = "Patient ID")]
        public string PatientID { get; set; }
        public string PatientID_Part1 { get; set; }
        public string PatientID_Part2 { get; set; }
        public string PatientID_Part3 { get; set; }
        [Display(Name = "Institution Name")]
        public string Site_Name { get; set; }
        [Required]
        [Display(Name = "Institution Number")]
        public string SiteID { get; set; }

        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Date Enrolled")]
        public DateTime? EntryDate { get; set; }
        [Display(Name = "Lab number")]
        public string Lab_Number { get; set; }
        public DateTime? FormBLKCreatedDate { get; set; }
        public DateTime? FormBLKDITConfirmedDate { get; set; }
        public DateTime? FormBLKLabReceiveddDate { get; set; }
        public bool? ScreenFailure { get; set; }
        public bool? Deceased { get; set; }
        [Display(Name = "Patient status")]
        public string Patient_Status { get; set; }
        public string Storage_Seq_Num { get; set; }

        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Consent Date")]
        public DateTime? Consent_Date_Shipped { get; set; }
        public DateTime? Created_on { get; set; }
        public int Created_by { get; set; }
        public DateTime? Modified_On { get; set; }
        public int Modified_By { get; set; }
        public string Record_Status { get; set; }
        public DateTime? FormBNK1CreatedDate { get; set; }
        public DateTime? FormBNK1LabReceiveddDate { get; set; }
        public DateTime? FormBNK2CreatedDate { get; set; }
        public DateTime? FormBNK2LabReceiveddDate { get; set; }
        public DateTime? FormBNK3CreatedDate { get; set; }
        public DateTime? FormBNK3LabReceiveddDate { get; set; }
        public DateTime? FormFTCreatedDate { get; set; }
        public String Form_Return { get; set; }
    }
}
