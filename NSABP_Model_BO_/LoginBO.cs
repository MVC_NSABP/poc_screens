﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSABP_Model_BO_
{
    public class LoginBO
    {
        //private string _UserName;
        //private string _Password;
        private int _UserID;
        //private string _ProtocolNumber;
        private int _RoleID;

        #region "Public Properties"
        /// <summary>
        /// Below are all public properties, created for getting the value from PL and can assign Insert and Update methods. 
        /// </summary>
        public string UserName { get; set; }
        public string Password { get; set; }

        public int RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        //public string Password;
        public int UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }
        public string ProtocolNumber { get; set; }
        public List<Protocol_BO> List_Protocols { get; set; }
        #endregion
    }
}
