﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NSABP_Model_BO_;
using NSABP_BLL;
using System.Data;
using System.Web.Security;

namespace NSABP_Product.Controllers
{

    public class UserLoginController : Controller
    {
       
        public ActionResult LoginPage()
        {
            //if (UserName == "Admin" && PWD == "nsabp")
            //{

            //    return RedirectToAction("RC_Create", "RC");
            //}           
            //else
            //{
            //    ViewBag.Message = "Invalid Details Failed to Login";
            //}

            return View();
        }
        [HttpPost]
        public ActionResult LoginPage(string UserName, String PWD)
        {
            if (UserName.ToLower().Trim() == "admin" && PWD == "nsabp")
            {
                Session["username"] = "Admin";
                Session["role"] = "Admin";
                return RedirectToAction("Dashboard", "Dashboard");
            }
            else if (UserName.ToLower().Trim() == "rcadmin" && PWD == "nsabp")
            {
                Session["username"] = "RC Admin";
                Session["role"] = "RC Admin";
                return RedirectToAction("RC_Create", "RC");
            }
            else if (UserName.ToLower().Trim() == "padmin" && PWD == "nsabp")
            {
                Session["username"] = "padmin";
                Session["role"] = "Protocol Manager";
                return RedirectToAction("ProtocolSetup", "ProtocolSetup");
            }
           
            else
            {
                ViewBag.Message = "Login Failed due to invalid credentials";
                return View();
            }
           
        }

        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            Session.Clear();
            Session.Abandon();
            return RedirectToAction("LoginPage", "UserLogin");
        } 
    }
}