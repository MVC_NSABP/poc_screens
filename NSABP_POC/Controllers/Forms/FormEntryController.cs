﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NSABP_Model_BO_;
using NSABP_BLL;
using System.ComponentModel.DataAnnotations;

namespace NSABP.Controllers.Forms
{
    public class FormEntryController : Controller
    {
 
        [HttpGet]
        public ActionResult FormEntry()
        {

            FormEntry_BO bo = new FormEntry_BO();
            bo.List_Sites = new List<Sites>();         
            for (int i = 0; i < 5; i++)
            {
                bo.List_Sites.Add(new Sites { SiteID = "7893-12" + i, Site_Name = "Site" + i });               
            }
            return View(bo);
        }
       
        [HttpGet]
        public ActionResult FormEntryDetails()
        {
            List<FormEntry_BO> bolist = new List<FormEntry_BO>();
            //try
            //{
            //    bolist = NSABP_BLL.FormEntry_BLL.GetFormEntry_Details(Convert.ToInt16(Session["Protocol_Id"]));
            //    FormEntry_BO bo = new FormEntry_BO();

            //    bo.List_Sites = Base_BLL.list_Sites();
               
            //}
            //catch (Exception ex)
            //{


            //}
            return View(bolist);

        }
        [HttpPost]
        public ActionResult FormEntryDetails(FormEntry_BO bo)
        {
            //bo.List_Sites = Base_BLL.list_Sites();
            return View(bo);
        }
        [HttpGet]
        public ActionResult FormEntryEdit(int ID)
        {
            FormEntry_BO bo = new FormEntry_BO();

            bo = FormEntry_BLL.GetFormEntry_Details_ByID(ID);
            bo.List_Sites = Base_BLL.list_Sites();

            //bo.SiteID = bo.Site_Name;
            string[] arrPatientID = bo.PatientID.Split('-');
          
            if (arrPatientID.Count() > 3)
            {
               
                bo.PatientID_Part2 = arrPatientID[arrPatientID.Length - 3];
                bo.PatientID_Part3 = arrPatientID[arrPatientID.Length - 2] + "-" + arrPatientID[arrPatientID.Length-1];
            }
           
            if (bo.Patient_Status != "")
            {

            }
            return PartialView("~/Views/FormEntry/PartialViews/P_FormEntry.cshtml", bo);
        }
        [HttpPost]
        //string name,
        public ActionResult FormEntryEdit(FormEntry_BO bo)
        {
            string status = "";
            string msg = "";
            try
            {
                bo.List_Sites = Base_BLL.list_Sites();               
                if (bo.Patient_Status == "Enrolled")
                    bo.ScreenFailure = false;
                else
                    bo.ScreenFailure = true;
                bo.Modified_By = 1;

                if (FormEntry_BLL.UpdateFormEntry(bo))
                {
                    status = "success";
                    msg = "updated successfuly.";
                }
                else
                {
                    status = "fail";
                    msg = "Invalid operation.";
                }

            }
            catch (Exception ex)
            {
                status = "fail";
                msg = "Invalid operation.";
                // throw;
            }
            return Json(new { status, msg });
        }
        public JsonResult getSiteName(string id)
        {
            try
            {
                FormFT_BO bomodel = new FormFT_BO();
                bomodel.listSites = Base_BLL.list_Sites();
                var sitename = bomodel.listSites.Where(x => x.SiteID == id).Select(x => x.Site_Name).FirstOrDefault();
                return Json(sitename);
            }
            catch (Exception ex)
            {
            }
            return Json(new { invalid = "" });
        }
        public ActionResult ShowPatientVisits()
        {
            return View();
        }
    }
}