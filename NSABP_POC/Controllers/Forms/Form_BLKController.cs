﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NSABP_Model_BO_;
using System.Data;
using NSABP_BLL;

namespace NSABP.Controllers.Forms.FormBLK
{
    public class Form_BLKController : Controller
    {

        [HttpGet]
        //[Authorize(Roles = "SiteUser")]
        public ActionResult FormBLK_Create()
        {
            ViewBag.formstatus = "create";
            
            FormBLK_BO bo = new FormBLK_BO();
            bo.List_Sites = new List<Sites>();
            bo.list_Patients = new List<Patients>();
            bo.list_Shipping_Company = new List<Shipping_Company>();
            for (int i = 0; i < 5; i++)
            {
                bo.List_Sites.Add(new Sites { SiteID = "7893-12" + i, Site_Name = "Site" + i });
                //bo.list_Patients.Add(new Patients { Patient_ID = "Patient_" + i });
                bo.list_Shipping_Company.Add(new Shipping_Company { ID = i, Company_name = "SC_" + i });

            }
            ViewBag.Patients = new SelectList(bo.list_Patients, "Patient_ID", "Patient_ID");

            return View(bo);
        }


        [HttpGet]
        public ActionResult FormBLK_Details()
        {

            ViewBag.formstatus = "edit";
            
            List<FormBLK_BO> bo = new List<FormBLK_BO>();
            return View(bo);
        }

        //to popup
        [HttpGet]
        public ActionResult EditFormBLK_Details(int id)
        {
            ViewBag.formstatus = "edit";
            FormBLK_BO bo = new FormBLK_BO();

            return PartialView("~/Views/Form_BLK/PartialViews/P_FormBLKCreate.cshtml", bo);
        }


        [HttpGet]

        public ActionResult GetFormBLK_DMG_Details()
        {
            Protocol_role_site bo = new Protocol_role_site();

            return View(FormBLK_BLL.FormBLKDetails(bo));
        }
        [HttpGet]
        public PartialViewResult FormBLK_DMG_Receive(int ID)
        {
            FormBLK_BO bo = new FormBLK_BO();

            return PartialView("~/Views/Form_BLK/PartialViews/FormBLK_DMG_Receive.cshtml", bo);
        }





    }
}