﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NSABP_POC.Controllers.SRMS
{
    public class DistributionProjectsController : Controller
    {
        // GET: DistributionProjects
      
        public ActionResult DistrubutionDetails()
        {
            return View();
        }
        [HttpGet]
        public PartialViewResult OpenManagePopup()
        {
           
            return PartialView("~/Views/DistributionProjects/MangeDetails.cshtml");
        }
        public ActionResult OPenManageSpecimens()
        {
            return PartialView("~/Views/DistributionProjects/ManageSpeciemens.cshtml");
        }
    }
}