﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Filters;

namespace NSABP_POC.Filters
{
    public class AuthAttribute : ActionFilterAttribute, IActionFilter, IExceptionFilter, IAuthenticationFilter
    {
        public void OnAuthentication(AuthenticationContext filterContext)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(filterContext.HttpContext.Session["UserName"])) || filterContext.ActionDescriptor.ActionName == "LoginPage")
            {
                // do nothing
            }
            else
            {
                filterContext.Result = new HttpUnauthorizedResult(); // mark unauthorized
            }          
        }

        //Runs after the OnAuthentication method  
        public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
        {
            if (filterContext.Result == null || filterContext.Result is HttpUnauthorizedResult)
            {
                filterContext.Result = new ViewResult
                {
                    ViewName = "Error",
                    ViewBag = { Message = "Your session has expired. Please login again.", Title = "Session Expired" }
                };
            }
            //TODO: Additional tasks on the request  
        }
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            //if (!string.IsNullOrEmpty(Convert.ToString(context.HttpContext.Session["UserName"])) || context.ActionDescriptor.ActionName == "LoginPage")
            //{

            //}          
        }

        public void OnException(ExceptionContext filterContext)
        {

            filterContext.Result = new ViewResult
            {
                ViewName = "Error",
                ViewBag = { Message = "Something went wrong", Title = "Error" }
            };
            var controller = filterContext.Controller as Controller;
            controller.HttpContext.Session.Clear();

        }
       
    }
}

