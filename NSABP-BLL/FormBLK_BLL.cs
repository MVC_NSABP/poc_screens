﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using NSABP_Model_BO_;
using NSABP_DAL;

namespace NSABP_BLL
{
    public class FormBLK_BLL
    {
        public static int SaveFormBLKDetails(FormBLK_BO bo)
        {
            int blk = 0;
            try
            {
                blk = FormBLK_DAL.SaveFormBLKDetails(bo);
            }
            catch (Exception ex)
            {
            }
            return blk;
        }
        public static bool SaveFormBLKShippemnt(FormBLK_BO bo)
        {
            bool isUpdate = false;
            try
            {
                isUpdate = FormBLK_DAL.SaveFormBLKShippemnt(bo);
            }
            catch (Exception ex)
            {
            }
            return isUpdate;
        }
        public static List<FormBLK_BO> FormBLKDetails(Protocol_role_site bo)
        {
            List<FormBLK_BO> list_FormBLK_BO = new List<FormBLK_BO>();
            try
            {
                list_FormBLK_BO = FormBLK_DAL.FormBLKDetails(bo);
            }
            catch (Exception)
            {

                throw;
            }
            return list_FormBLK_BO;
        }
        public static FormBLK_BO FormBLKDetails(int id)
        {
            FormBLK_BO bo = new FormBLK_BO();
            try
            {
                bo = FormBLK_DAL.FormBLKDetails(id);
            }
            catch (Exception)
            {

                throw;
            }
            return bo;
        }

        public static bool EditFormBLKDetails(FormBLK_BO bo)
        {
            bool isave = false;
            try
            {
                isave = FormBLK_DAL.EditFormBLKDetails(bo);
            }
            catch
            {
                isave = false;
                throw;
            }
            return isave;
        }

        public static bool ReceiveFormBLKDMG(FormBLK_BO bo)
        {
            bool isave = false;
            try
            {
                isave = FormBLK_DAL.ReceiveFormBLKDMG(bo);
            }
            catch
            {
                isave = false;
                throw;
            }
            return isave;
        }

        public static List<FormBLK_Blocks_BO> BlockSlideDetails(int frmblkid)
        {
            List<FormBLK_Blocks_BO> list_FormBLK_Blocks_BO = new List<FormBLK_Blocks_BO>();
            try
            {
                list_FormBLK_Blocks_BO = FormBLK_DAL.BlockSlideDetails(frmblkid);
            }
            catch (Exception)
            {

                throw;
            }
            return list_FormBLK_Blocks_BO;
        }
    }

}


