﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using NSABP_DAL;
using NSABP_Model_BO_;


namespace NSABP_BLL
{
    public class User_BLL
    {
        //public static DataTable UserStatus()
        //{
        //    DataTable dt = new DataTable();
        //    try
        //    {
        //        dt = User_DAL.UserStatus();
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    return dt;
        //}
        //public static DataTable Role()
        //{
        //    DataTable dt = new DataTable();
        //    try
        //    {
        //        dt = User_DAL.Role();

        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //    return dt;

        //}
        public static DataTable GetProtocols()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = User_DAL.GetProtocols();
            }
            catch (Exception ex)
            {
            }
            return dt;
        }


        public static bool SaveUserDetails(User_BO bo)
        {
            bool isSave = false;
            try
            {
                isSave = User_DAL.SaveUserDetails(bo);
            }
            catch (Exception ex)
            {

            }
            return isSave;
        }

        public static DataSet GetProtocols_Roles_Status()
        {
            DataSet ds = new DataSet();
            try
            {
                ds = User_DAL.GetProtocols_Roles_Status();
            }
            catch (Exception)
            {
            }
            return ds;
        }
        public static bool UpdateUserDetails(User_BO bo)
        {
            bool isUpdate = false;
            try
            {
                isUpdate = User_DAL.UpdateUserDetails(bo);
            }
            catch (Exception ex)
            {
            }
            return isUpdate;
        }
        public static List<User_BO> GetAllUsers()
        {
            List<User_BO> list_user_bo = new List<User_BO>();
            try
            {
                list_user_bo = User_DAL.GetAllUsers();
            }
            catch (Exception ex)
            {
            }
            return list_user_bo;
        }

        public static List<Protocol_role_site> ListProtocol_role_site(int userID)
        {
            List<Protocol_role_site> list_user_bo = new List<Protocol_role_site>();
            try
            {
                list_user_bo = User_DAL.ListProtocol_role_site(userID);
            }
            catch (Exception ex)
            {
            }
            return list_user_bo;
        }

        public static bool IsUserExists(string username)
        {
            bool isExists = false;
            try
            {
                isExists = User_DAL.IsUserExists(username);
            }
            catch (Exception)
            {
            }
            return isExists;
        }

        public static bool Changepwd(string pwd, int UserId)
        {
            bool issave = false;
            try
            {
                issave = User_DAL.Changepwd(pwd, UserId);
            }
            catch (Exception ex)
            {
            }
            return issave;
        }
    }

}
