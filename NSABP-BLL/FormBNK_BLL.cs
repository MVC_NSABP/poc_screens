﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using NSABP_DAL;
using NSABP_Model_BO_;

namespace NSABP_BLL
{
    public class FormBNK_BLL
    {
        public static bool SaveBNKDetails(FormBNK_BO bo)
        {
            bool isSave = false;
            try
            {
                isSave = FormBNK_DAL.SaveBNKDetails(bo);
            }
            catch (Exception ex)
            {

            }
            return isSave;
        }
        public static bool UpdateUserDetails(FormBNK_BO bo)
        {
            bool isUpdate = false;
            try
            {
                isUpdate = FormBNK_DAL.UpdateUserDetails(bo);
            }
            catch (Exception ex)
            {
            }
            return isUpdate;
        }
        public static List<FormBNK_BO> GetFormBNK_Details(string Role, string Site_ID)
        {
            List<FormBNK_BO> List_BNK_Details = new List<FormBNK_BO>();
            try
            {
                List_BNK_Details = FormBNK_DAL.GetFormBNK_Details(Role, Site_ID);
            }
            catch (Exception ex)
            {
            }
            return List_BNK_Details;
        }
    }
}
