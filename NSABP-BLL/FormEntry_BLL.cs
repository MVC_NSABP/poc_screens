﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSABP_Model_BO_;
using NSABP_DAL;
using System.Data;
namespace NSABP_BLL
{
    public class FormEntry_BLL
    {
        public static bool SaveFormEntryDetails(FormEntry_BO bo)
        {
            bool isSave = false;
            try
            {
                isSave = FormEntry_DAL.SaveFormEntryDetails(bo);
            }
            catch (Exception ex)
            {

            }
            return isSave;
        }
        public static bool UpdateFormEntry(FormEntry_BO bo)
        {
            bool isUpdate = false;
            try
            {
                isUpdate = FormEntry_DAL.UpdateFormEntry(bo);

            }
            catch (Exception ex)
            {
            }
            return isUpdate;
        }
        public static List<FormEntry_BO> GetFormEntry_Details(int prtocolID)
        {
            List<FormEntry_BO> List_BNK_Details = new List<FormEntry_BO>();
            try
            {
                List_BNK_Details = FormEntry_DAL.GetFormEntry_Details(prtocolID);
            }
            catch (Exception ex)
            {
            }
            return List_BNK_Details;
        }
        public static FormEntry_BO GetFormEntry_Details_ByID(int ID)
        {
            FormEntry_BO bo = new FormEntry_BO();
            try
            {
                bo = FormEntry_DAL.GetFormEntry_Details_ByID(ID);
            }
            catch (Exception ex)
            {
            }
            return bo;
        }
        public static bool IsPatient_IDExists(string Patient_ID)
        {
            bool isExists = false;
            try
            {
                isExists = FormEntry_DAL.IsPatient_IDExists(Patient_ID);

            }
            catch (Exception ex)
            {
            }
            return isExists;
        }
    }
}
