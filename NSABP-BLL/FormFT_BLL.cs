﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSABP_Model_BO_;
using NSABP_DAL;
using System.Data;
namespace NSABP_BLL
{
    public class FormFT_BLL
    {
        public static bool SaveFormFTDetails(FormFT_BO bo)
        {
            bool isSave = false;
            try
            {
                isSave = FormFT_DAL.SaveFormFTDetails(bo);
            }
            catch (Exception ex)
            {

            }
            return isSave;
        }
        public static bool SaveShipmentDetails(FormFT_BO bo)
        {
            bool isSave = false;
            try
            {
                isSave = FormFT_DAL.SaveShipmentDetails(bo);
            }
            catch (Exception ex)
            {

            }
            return isSave;
        }
        public static DataTable SiteOfOtherTumorTissues()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = FormFT_DAL.SiteOfOtherTumorTissues();
            }
            catch (Exception ex)
            {
            }
            return dt;
        }
        public static List<FormFT_BO> getFormFTDetailsByUserID(string department, string role)
        {
            List<FormFT_BO> listFormFt = new List<FormFT_BO>();
            try
            {
                listFormFt = FormFT_DAL.getFormFTDetailsByUserID(department, role);
            }
            catch (Exception ex)
            {

            }
            return listFormFt;
        }
        public static List<FormFT_BO> getFormFtDetailsbyFormFtid(int Id)
        {
            List<FormFT_BO> listFormFt = new List<FormFT_BO>();
            try
            {
                listFormFt = FormFT_DAL.getFormFtDetailsbyFormFtid(Id);
            }
            catch (Exception ex)
            {
            }
            return listFormFt;
        }
        public static bool UpdateFormFt(FormFT_BO bo)
        {
            bool isUpdate = false;
            try
            {
                isUpdate = FormFT_DAL.UpdateFormFt(bo);
            }
            catch (Exception ex)
            {

            }
            return isUpdate;
        }
        public static DataTable GetSlides_Locations()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = FormFT_DAL.GetSlides_Locations();
            }
            catch (Exception ex)
            {
            }
            return dt;
        }
        public static DataTable GetReceievedConditions()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = FormFT_DAL.GetReceievedConditions();
            }
            catch (Exception ex)
            {

            }
            return dt;
        }
        public static DataTable GetSlides_Status()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = FormFT_DAL.GetSlides_Status();
            }
            catch (Exception ex)
            {
            }
            return dt;
        }
        public static FormFT_BO GetReceivedDetailsforDMG(int id)
        {
            FormFT_BO bo = new FormFT_BO();
            try
            {
                bo = FormFT_DAL.GetReceivedDetailsforDMG(id);
            }
            catch (Exception ex)
            {

            }
            return bo;
        }
        public static bool saveReceivedDetails(FormFT_BO bo)
        {
            bool isSave = false;
            try
            {
                isSave = FormFT_DAL.saveReceivedDetails(bo);
            }
            catch (Exception ex)
            {
            }
            return isSave;
        }

        public static List<FormFT_BO> getFormFTDetailsByUserID(Protocol_role_site boprs)
        {
            List<FormFT_BO> listFormFt = new List<FormFT_BO>();
            try
            {
                listFormFt = FormFT_DAL.getFormFTDetailsByUserID(boprs);
            }
            catch (Exception ex)
            {

            }
            return listFormFt;
        }
        //new 
        public static bool SaveFormFTDetails_TimePoint(FormFT_BO bo, int tpId)
        {
            bool isSave = false;
            try
            {
                isSave = FormFT_DAL.SaveFormFTDetails_TimePoint(bo, tpId);

            }
            catch (Exception ex)
            {

            }
            return isSave;
        }

        public static List<FormFT_Timepoint> GetRemaining_TimePoints_Form_Protocol(int _protocolId, int _formId, List<int> Timepointid)
        {
            List<FormFT_Timepoint> listTimepoints = new List<FormFT_Timepoint>();
            try
            {
                listTimepoints = FormFT_DAL.GetRemaining_TimePoints_Form_Protocol(_protocolId, _formId, Timepointid);
            }
            catch (Exception ex)
            {
            }
            return listTimepoints;
        }

        public static bool SaveShipmentDetails_withTimpoints(FormFT_BO bo)
        {
            bool isSave = false;
            try
            {
                isSave = FormFT_DAL.SaveShipmentDetails_withTimpoints(bo);
            }
            catch (Exception ex)
            {

            }
            return isSave;
        }
        public static List<Patients> list_Patients(string SiteID, string formname, int Protocolid)
        {
            List<Patients> list_result = new List<Patients>();
            try
            {
                list_result = FormFT_DAL.list_Patients(SiteID, formname, Protocolid);

            }
            catch (Exception ex)
            {
            }
            return list_result;
        }

        public static List<FormFT_BO> getFormFtDetailsbyFormFtid_TP(int Id)
        {
            List<FormFT_BO> listFormFt = new List<FormFT_BO>();
            try
            {
                listFormFt = FormFT_DAL.getFormFtDetailsbyFormFtid_TP(Id);
            }
            catch (Exception ex)
            {
            }
            return listFormFt;
        }
    }
}

