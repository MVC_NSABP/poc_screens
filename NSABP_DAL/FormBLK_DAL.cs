﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using NSABP_Model_BO_;

namespace NSABP_DAL
{
    public class FormBLK_DAL
    {
        private static string Co = ConfigurationManager.ConnectionStrings["Con"].ConnectionString;
        private static SqlConnection Conn;
        private static StringBuilder sb_qry;
        public static int SaveFormBLKDetails(FormBLK_BO bo)
        {
            int blkid = 0;
            try
            {
                Conn = new SqlConnection(Co);
                sb_qry = new StringBuilder();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = Conn;
                sb_qry.Append("INSERT INTO T2_Form_BLK_PhaseII ([Form_BlockID],[T1_PatientID],[FormBLK_PersonCompletingForm_LName],[FormBLK_PersonCompletingForm_FName],[FormBLK_PersonCompletingForm_Phone],[FormBLK_PersonCompletingForm_Email],[FormBLK_Patology_Accession_Number],[isFromDiagnosticBreastBiopsy],[FPPE_FromTheDiagnostiBreastBiopsySpecimen],");
                sb_qry.Append("[UnstainedSlidesFromDiagnosticBreastBiopsy],[numberOfSlides],[numberOfBlocks],[Created_By],[Created_On],[Modified_By],[Modified_On],[Record_Status]) VALUES(");
                bo.Form_BlockID = bo.T1_PatientID + "-BLK";
                sb_qry.Append(" '" + bo.Form_BlockID + "','" + bo.T1_PatientID + "','" + bo.FormBLK_PersonCompletingForm_LName + "','" + bo.FormBLK_PersonCompletingForm_FName + "','" + bo.FormBLK_PersonCompletingForm_Phone + "','" + bo.FormBLK_PersonCompletingForm_Email + "','" + bo.FormBLK_Patology_Accession_Number + "',");
                sb_qry.Append(" '" + bo.isFromDiagnosticBreastBiopsy + "','" + bo.FPPE_FromTheDiagnostiBreastBiopsySpecimen + "'");
                sb_qry.Append(" ,'" + bo.UnstainedSlidesFromDiagnosticBreastBiopsy + "',");
                if (bo.numberOfSlides > 0)
                    sb_qry.Append("  " + bo.numberOfSlides + ",");
                else
                    sb_qry.Append("null,");
                if (bo.numberOfBlocks > 0)
                    sb_qry.Append("   '" + bo.numberOfBlocks + "',");
                else
                    sb_qry.Append("null,");
                sb_qry.Append(" " + bo.Created_By + ",getdate(),1,getdate(),'Active' ) select SCOPE_IDENTITY() ");

                cmd.CommandText = sb_qry.ToString();
                Conn.Open();
                blkid = Convert.ToInt16(cmd.ExecuteScalar());


                sb_qry.Clear();
                cmd.CommandText = "update[T9_Patients] set FormBLKCreatedDate = GETDATE() where Patient_ID = '" + bo.T1_PatientID + "'";
                cmd.ExecuteNonQuery();

                if (bo.numberOfSlides > 0)
                {
                    //    sb_qry.Append(" INSERT INTO[dbo].[T22_slide_Details_PhaseII]([Slide_ID],[T21_PhaseII_Block_ID],[T50_Record_Created_By],[Created_On],[Record_Status]) VALUES ( ");
                    //    sb_qry.Append(" '" + bo.Slide_ID + "-" + i + "'," + blkid + "," + bo.Created_By + ",getdate(),'Active' ");
                    sb_qry.Append("INSERT INTO [dbo].[T21_Block_slide_Details_PhaseII] ([Block_Slide_ID],[T2_PhaseII_FormBlk_ID] ,[T50_Record_Created_By],[Created_On],[Record_Status])VALUES( ");
                    sb_qry.Append(" '" + bo.List_Blocks[0].Block_Slide_ID + "'," + blkid + "," + bo.Created_By + ",getdate(),'Active') ");

                    cmd.CommandText = sb_qry.ToString();
                    cmd.ExecuteNonQuery();
                    sb_qry.Clear();
                }
                if (bo.numberOfBlocks > 0)
                    for (int i = 0; i < bo.numberOfBlocks; i++)
                    {
                        sb_qry.Append("INSERT INTO [dbo].[T21_Block_slide_Details_PhaseII] ([Block_Slide_ID],[T2_PhaseII_FormBlk_ID] ,[T50_Record_Created_By],[Created_On],[Record_Status])VALUES( ");
                        sb_qry.Append(" '" + bo.List_Blocks[i].Block_Slide_ID + "'," + blkid + "," + bo.Created_By + ",getdate(),'Active') ");
                        cmd.CommandText = sb_qry.ToString();
                        cmd.ExecuteNonQuery();
                        sb_qry.Clear();
                    }


            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                Conn.Close();
            }
            return blkid;
        }
        public static bool SaveFormBLKShippemnt(FormBLK_BO bo)
        {
            bool isSave = false;
            try
            {
                Conn = new SqlConnection(Co);
                sb_qry = new StringBuilder();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = Conn;
                sb_qry.Append(" update [T2_Form_BLK_PhaseII] set [dateShippedToDivisionOfPathology]='" + bo.dateShippedToDivisionOfPathology + "',[shippingCompanyID]=" + bo.shippingCompanyID + ", [trackingNumber]='" + bo.trackingNumber + "' where Form_BlockID='" + bo.Form_BlockID + "'");
                cmd.CommandText = sb_qry.ToString();
                Conn.Open();
                cmd.ExecuteNonQuery();

                isSave = true;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                Conn.Close();
            }
            return isSave;
        }
        public static bool EditFormBLKDetails(FormBLK_BO bo)
        {
            bool isave = false;
            try
            {
                Conn = new SqlConnection(Co);
                sb_qry = new StringBuilder();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = Conn;

                sb_qry.Append(" UPDATE [T2_Form_BLK_PhaseII] SET [isFromDiagnosticBreastBiopsy] = '" + bo.isFromDiagnosticBreastBiopsy + "',[FPPE_FromTheDiagnostiBreastBiopsySpecimen] = '" + bo.FPPE_FromTheDiagnostiBreastBiopsySpecimen + "',[UnstainedSlidesFromDiagnosticBreastBiopsy] = '" + bo.UnstainedSlidesFromDiagnosticBreastBiopsy + "', [numberOfSlides] =  ");
                if (bo.numberOfSlides > 0)
                    sb_qry.Append(" " + bo.numberOfSlides + ",");
                else
                    sb_qry.Append(" null,");
                if (bo.numberOfBlocks > 0)
                    sb_qry.Append(" [numberOfBlocks] = " + bo.numberOfBlocks + ",");
                else
                    sb_qry.Append(" [numberOfBlocks] = null,");
                sb_qry.Append("[Modified_By] = 1,[Modified_On] = getdate() where Id=" + bo.ID + " ");
                cmd.CommandText = sb_qry.ToString();
                Conn.Open();
                cmd.ExecuteNonQuery();
                sb_qry.Clear();
                isave = true;
                //if (bo.numberOfSlides > 0)
                //{
                //    //    sb_qry.Append(" INSERT INTO[dbo].[T22_slide_Details_PhaseII]([Slide_ID],[T21_PhaseII_Block_ID],[T50_Record_Created_By],[Created_On],[Record_Status]) VALUES ( ");
                //    //    sb_qry.Append(" '" + bo.Slide_ID + "-" + i + "'," + blkid + "," + bo.Created_By + ",getdate(),'Active' ");
                //    sb_qry.Append("INSERT INTO [dbo].[T21_Block_slide_Details_PhaseII] ([Block_Slide_ID],[T2_PhaseII_FormBlk_ID] ,[T50_Record_Created_By],[Created_On],[Record_Status])VALUES( ");
                //    sb_qry.Append(" '" + bo.List_Blocks[0].Block_Slide_ID + "'," + blkid + "," + bo.Created_By + ",getdate(),'Active') ");

                //    cmd.CommandText = sb_qry.ToString();
                //    cmd.ExecuteNonQuery();
                //}
                //if (bo.numberOfBlocks > 0)
                //    for (int i = 0; i < bo.numberOfBlocks; i++)
                //    {
                //        sb_qry.Append("INSERT INTO [dbo].[T21_Block_slide_Details_PhaseII] ([Block_Slide_ID],[T2_PhaseII_FormBlk_ID] ,[T50_Record_Created_By],[Created_On],[Record_Status])VALUES( ");
                //        sb_qry.Append(" '" + bo.List_Blocks[i].Block_Slide_ID + "'," + blkid + "," + bo.Created_By + ",getdate(),'Active') ");
                //        cmd.CommandText = sb_qry.ToString();
                //        cmd.ExecuteNonQuery();
                //    }


            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                Conn.Close();
            }
            return isave;
        }
        public static List<FormBLK_BO> FormBLKDetails(Protocol_role_site PRS)
        {
            List<FormBLK_BO> list_FormBLK_BO = new List<FormBLK_BO>();
            try
            {
                Conn = new SqlConnection(Co);
                sb_qry = new StringBuilder();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = Conn;
                sb_qry.Append(" SELECT distinct  T2.[ID],[Form_BlockID],t8.Site_Name,t8.SiteID,[T1_PatientID],[FormBLK_PersonCompletingForm_LName],[FormBLK_PersonCompletingForm_FName],");
                sb_qry.Append(" [FormBLK_PersonCompletingForm_Phone],[FormBLK_PersonCompletingForm_Email],[FormBLK_Patology_Accession_Number],");
                sb_qry.Append(" [isFromDiagnosticBreastBiopsy],[FPPE_FromTheDiagnostiBreastBiopsySpecimen],[UnstainedSlidesFromDiagnosticBreastBiopsy],");
                sb_qry.Append(" [NSABP_Patology_Accession_Number],[numberOfSlides],[numberOfBlocks],[Notes],[DMG_Receieved_By], t1a.FName + T1A.LName as");
                sb_qry.Append(" [DMG_Receieved_By_Name],t1.FName+t1.LName as Created_By_Name,[DMG_Receieved_Date],[Received_Condition],[shippingCompanyID], t50.Company_name,");
                sb_qry.Append(" [trackingNumber],[dateShippedToDivisionOfPathology], T2.[Created_By], T2.[Created_On], t2.[Modified_By], t2.[Modified_On],");
                sb_qry.Append(" [DateDiagnosticBreastBiopsyProcedure],[FormEnclosed],[numberOfSlidesReceivedByNSABP],[numberOfBlocksReceivedByNSABP],");
                sb_qry.Append(" [IsFormBlockReceived],[AllowSiteEdit],[Receipt_Number],[AllowSiteEditDate], T2.[Record_Status],t9.Lab_Number,t9.Storage_Seq_Num");
                sb_qry.Append(" FROM[dbo].[T2_Form_BLK_PhaseII] T2");
                sb_qry.Append(" left join T50_Shipping_Company t50 on(t2.shippingCompanyID = t50.ID)");
                sb_qry.Append(" left join[T1_User_Main] T1 on(t1.User_ID = t2.Created_By and t1.Record_Status = 'Active')");
                sb_qry.Append(" left join[T1_User_Main] T1A on(t1A.User_ID = t2.DMG_Receieved_By and  t1A.Record_Status = 'Active')");
                sb_qry.Append(" join T9_Patients t9 on(t9.Patient_ID = t2.T1_PatientID  )");
                sb_qry.Append(" join[dbo].[T8_Sites] t8 on(t8.SiteID = t9.Institution)");
                sb_qry.Append(" join[dbo].[T101_Map_Protocol_User_Role] t101 on (t101.T2_Protocol_Id = t9.Protocol_Id)");
                sb_qry.Append(" join[dbo].[T5_User_Roles] t5 on(t5.ID = t101.T5_Role_ID) ");

                sb_qry.Append(" where T2.Record_Status = 'Active' and t9.Protocol_Id="+PRS.ProtocolID+"  ");
                if (PRS.Role_Name == "DMG")
                {
                    sb_qry.Append(" and ( T50.[Company_name] is Not Null or [trackingNumber] is Not Null or [dateShippedToDivisionOfPathology] is Not Null)");
                }
                else if (PRS.Role_Name == "SiteUser")
                {
                    sb_qry.Append("and  T9.Institution like '" + PRS.T8_sites_Id.Substring(0, PRS.T8_sites_Id.IndexOf("-")) + "-%'");
                }
                cmd.CommandText = sb_qry.ToString();
                Conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    FormBLK_BO bo = new FormBLK_BO();
                    bo.ID = Convert.ToInt16(dr["ID"]);
                    bo.Lab_Number = dr["Lab_Number"].ToString();
                    if (dr["Storage_Seq_Num"] != DBNull.Value)
                        bo.Storage_Seq_Num = dr["Storage_Seq_Num"].ToString();
                    bo.Form_BlockID = dr["Form_BlockID"].ToString();
                    bo.Site_Name = dr["Site_Name"].ToString();
                    bo.SiteID = dr["SiteID"].ToString();
                    bo.T1_PatientID = dr["T1_PatientID"].ToString();
                    bo.FormBLK_PersonCompletingForm_LName = dr["FormBLK_PersonCompletingForm_LName"].ToString();
                    bo.FormBLK_PersonCompletingForm_FName = dr["FormBLK_PersonCompletingForm_FName"].ToString();
                    bo.FormBLK_PersonCompletingForm_Phone = dr["FormBLK_PersonCompletingForm_Phone"].ToString();
                    bo.FormBLK_PersonCompletingForm_Email = dr["FormBLK_PersonCompletingForm_Email"].ToString();
                    bo.FormBLK_Patology_Accession_Number = dr["FormBLK_Patology_Accession_Number"].ToString();
                    bo.isFromDiagnosticBreastBiopsy = (bool?)dr["isFromDiagnosticBreastBiopsy"];
                    bo.FPPE_FromTheDiagnostiBreastBiopsySpecimen = (bool)dr["FPPE_FromTheDiagnostiBreastBiopsySpecimen"];
                    bo.UnstainedSlidesFromDiagnosticBreastBiopsy = (bool)dr["UnstainedSlidesFromDiagnosticBreastBiopsy"];
                    bo.NSABP_Patology_Accession_Number = dr["NSABP_Patology_Accession_Number"].ToString();
                    if (dr["numberOfSlides"] != DBNull.Value)
                        bo.numberOfSlides = Convert.ToInt16(dr["numberOfSlides"]);
                    if (dr["numberOfBlocks"] != DBNull.Value)
                        bo.numberOfBlocks = Convert.ToInt16(dr["numberOfBlocks"]);
                    bo.Notes = dr["Notes"].ToString();
                    if (dr["DMG_Receieved_By"] != DBNull.Value)
                        bo.DMG_Receieved_By = Convert.ToInt16(dr["DMG_Receieved_By"]);
                    bo.DMG_Receieved_By_Name = dr["DMG_Receieved_By_Name"].ToString();
                    if (dr["DMG_Receieved_Date"] != DBNull.Value)
                        bo.DMG_Receieved_Date = Convert.ToDateTime(dr["DMG_Receieved_Date"]);
                    //if (dr["Received_Condition"] != DBNull.Value)
                    //    bo.Received_Condition = Convert.ToInt16(dr["Received_Condition"]);
                    if (dr["shippingCompanyID"] != DBNull.Value)
                        bo.shippingCompanyID = Convert.ToInt16(dr["shippingCompanyID"]);
                    bo.Company_name = dr["Company_name"].ToString();
                    bo.trackingNumber = dr["trackingNumber"].ToString();
                    if (dr["dateShippedToDivisionOfPathology"] != DBNull.Value)
                        bo.dateShippedToDivisionOfPathology = Convert.ToDateTime(dr["dateShippedToDivisionOfPathology"]);
                    bo.Created_By = Convert.ToInt16(dr["Created_By"]);
                    bo.Created_On = (DateTime)dr["Created_On"];
                    bo.Created_By_Name = dr["Created_By_Name"].ToString();
                    list_FormBLK_BO.Add(bo);
                }
                Conn.Close();

            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                Conn.Close();
            }
            return list_FormBLK_BO;
        }
        public static FormBLK_BO FormBLKDetails(int id)
        {
            FormBLK_BO bo = new FormBLK_BO();
            try
            {
                Conn = new SqlConnection(Co);
                sb_qry = new StringBuilder();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = Conn;
                sb_qry.Append(" SELECT  T2.[ID],[Form_BlockID],t8.Site_Name,t8.SiteID,[T1_PatientID],[FormBLK_PersonCompletingForm_LName],[FormBLK_PersonCompletingForm_FName],");
                sb_qry.Append(" [FormBLK_PersonCompletingForm_Phone],[FormBLK_PersonCompletingForm_Email],[FormBLK_Patology_Accession_Number],");
                sb_qry.Append(" [isFromDiagnosticBreastBiopsy],[FPPE_FromTheDiagnostiBreastBiopsySpecimen],[UnstainedSlidesFromDiagnosticBreastBiopsy],");
                sb_qry.Append(" [NSABP_Patology_Accession_Number],[numberOfSlides],[numberOfBlocks],[Notes],[DMG_Receieved_By], t1a.FName + T1A.LName as");
                sb_qry.Append(" [DMG_Receieved_By_Name],t1.FName+t1.LName as Created_By_Name,[DMG_Receieved_Date],[Received_Condition],[shippingCompanyID], t50.Company_name,");
                sb_qry.Append(" [trackingNumber],[dateShippedToDivisionOfPathology], T2.[Created_By], T2.[Created_On], t2.[Modified_By], t2.[Modified_On],");
                sb_qry.Append(" [DateDiagnosticBreastBiopsyProcedure],[FormEnclosed],[numberOfSlidesReceivedByNSABP],[numberOfBlocksReceivedByNSABP],");
                sb_qry.Append(" [IsFormBlockReceived],[AllowSiteEdit],[Receipt_Number],[AllowSiteEditDate], T2.[Record_Status],t9.Lab_Number,t9.Storage_Seq_Num");
                sb_qry.Append(" FROM[dbo].[T2_Form_BLK_PhaseII] T2");
                sb_qry.Append(" left join T50_Shipping_Company t50 on(t2.shippingCompanyID = t50.ID)");
                sb_qry.Append(" left join[T1_User_Main] T1 on(t1.User_ID = t2.Created_By and t1.Record_Status = 'Active')");
                sb_qry.Append(" left join[T1_User_Main] T1A on(t1A.User_ID = t2.DMG_Receieved_By and  t1A.Record_Status = 'Active')");
                sb_qry.Append(" join T9_Patients t9 on(t9.Patient_ID = t2.T1_PatientID)");
                sb_qry.Append(" join[dbo].[T8_Sites] t8 on(t8.SiteID = t9.Institution)");
                sb_qry.Append(" where T2.Record_Status = 'Active' and T2.[ID]=" + id + " ");
                cmd.CommandText = sb_qry.ToString();
                Conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {

                    bo.ID = Convert.ToInt16(dr["ID"]);
                    bo.Form_BlockID = dr["Form_BlockID"].ToString();
                    bo.Site_Name = dr["Site_Name"].ToString();
                    bo.SiteID = dr["SiteID"].ToString();
                    bo.T1_PatientID = dr["T1_PatientID"].ToString();
                    bo.FormBLK_PersonCompletingForm_LName = dr["FormBLK_PersonCompletingForm_LName"].ToString();
                    bo.FormBLK_PersonCompletingForm_FName = dr["FormBLK_PersonCompletingForm_FName"].ToString();
                    bo.FormBLK_PersonCompletingForm_Phone = dr["FormBLK_PersonCompletingForm_Phone"].ToString();
                    bo.FormBLK_PersonCompletingForm_Email = dr["FormBLK_PersonCompletingForm_Email"].ToString();
                    bo.FormBLK_Patology_Accession_Number = dr["FormBLK_Patology_Accession_Number"].ToString();
                    bo.isFromDiagnosticBreastBiopsy = (bool?)dr["isFromDiagnosticBreastBiopsy"];
                    bo.FPPE_FromTheDiagnostiBreastBiopsySpecimen = (bool)dr["FPPE_FromTheDiagnostiBreastBiopsySpecimen"];
                    bo.UnstainedSlidesFromDiagnosticBreastBiopsy = (bool)dr["UnstainedSlidesFromDiagnosticBreastBiopsy"];
                    bo.NSABP_Patology_Accession_Number = dr["NSABP_Patology_Accession_Number"].ToString();
                    if (dr["numberOfSlides"] != DBNull.Value)
                        bo.numberOfSlides = Convert.ToInt16(dr["numberOfSlides"]);
                    if (dr["numberOfBlocks"] != DBNull.Value)
                        bo.numberOfBlocks = Convert.ToInt16(dr["numberOfBlocks"]);
                    bo.Notes = dr["Notes"].ToString();

                    if (dr["DMG_Receieved_By"] != DBNull.Value)
                        bo.DMG_Receieved_By = Convert.ToInt16(dr["DMG_Receieved_By"]);
                    bo.DMG_Receieved_By_Name = dr["DMG_Receieved_By_Name"].ToString();
                    if (dr["DMG_Receieved_Date"] != DBNull.Value)
                        bo.DMG_Receieved_Date = Convert.ToDateTime(dr["DMG_Receieved_Date"]);
                    //if (dr["Received_Condition"] != DBNull.Value)
                    //    bo.Received_Condition = Convert.ToInt16(dr["Received_Condition"]);
                    if (dr["shippingCompanyID"] != DBNull.Value)
                        bo.shippingCompanyID = Convert.ToInt16(dr["shippingCompanyID"]);
                    bo.Company_name = dr["Company_name"].ToString();
                    bo.trackingNumber = dr["trackingNumber"].ToString();
                    if (dr["dateShippedToDivisionOfPathology"] != DBNull.Value)
                        bo.dateShippedToDivisionOfPathology = Convert.ToDateTime(dr["dateShippedToDivisionOfPathology"]);
                    if (dr["Lab_Number"] != DBNull.Value)
                        bo.Lab_Number = dr["Lab_Number"].ToString();
                    if (dr["Storage_Seq_Num"] != DBNull.Value)
                        bo.Lab_Number = dr["Storage_Seq_Num"].ToString();

                }
                Conn.Close();

            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                Conn.Close();
            }
            return bo;
        }

        public static bool ReceiveFormBLKDMG(FormBLK_BO bo)
        {
            bool issave = false;
            try
            {
                Conn = new SqlConnection(Co);
                sb_qry = new StringBuilder();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = Conn;
                Conn.Open();
                //sb_qry.Append("INSERT INTO [dbo].[T21_Block_slide_Details_PhaseII] ([Block_Slide_ID],[T2_PhaseII_FormBlk_ID],[Receieved_condition],[Pathology_note],[Re_embed],[T50_Record_Created_By],[Created_On],[T50_Record_Modified_By],[Modified_On],[Record_Status],[Dmg_Block_ID],[isBlockReceived]) ");
                //sb_qry.Append(" VALUES ("+bo.T2_PhaseII_FormBlk_ID+",)");   
                DataTable dt = new DataTable();
                cmd.CommandText = "SELECT  [ID],[Block_Slide_ID],[T2_PhaseII_FormBlk_ID] FROM [T21_Block_slide_Details_PhaseII] where [T2_PhaseII_FormBlk_ID]=" + bo.ID + "";
                SqlDataAdapter dr = new SqlDataAdapter(cmd);
                dr.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count && i < bo.List_Blocks.Count; i++)
                    {

                        sb_qry.Append(" UPDATE [dbo].[T21_Block_slide_Details_PhaseII] SET [Receieved_condition] = " + bo.List_Blocks[i].Receieved_condition + ", ");
                        sb_qry.Append("[Pathology_note] = '" + bo.Notes + "', ");
                        sb_qry.Append("[Re_embed] = '" + bo.List_Blocks[i].Re_embed + "' ,");
                        sb_qry.Append("[T50_Record_Created_By] = '" + bo.Created_By + "', ");
                        sb_qry.Append("[Created_On] = getdate() , ");
                        sb_qry.Append("[Dmg_Block_ID] = '" + bo.List_Blocks[i].Dmg_Block_ID + "', ");
                        sb_qry.Append("[isBlockReceived] = 1 ");
                        sb_qry.Append("WHERE [ID] = '" + dt.Rows[i]["ID"].ToString() + "' ");
                        cmd.CommandText = sb_qry.ToString();
                        cmd.ExecuteNonQuery();
                        sb_qry.Clear();
                    }
                }
                cmd.CommandText = "UPDATE[dbo].[T2_Form_BLK_PhaseII] SET [DMG_Receieved_By] ='" + bo.DMG_Receieved_By + "' ,[DMG_Receieved_Date] ='" + bo.DMG_Receieved_Date + "' WHERE ID = " + bo.ID + "";
                cmd.ExecuteNonQuery();
                issave = true;
                //if (bo.numberOfSlides > 0)
                //{
                //    //    sb_qry.Append(" INSERT INTO[dbo].[T22_slide_Details_PhaseII]([Slide_ID],[T21_PhaseII_Block_ID],[T50_Record_Created_By],[Created_On],[Record_Status]) VALUES ( ");
                //    //    sb_qry.Append(" '" + bo.Slide_ID + "-" + i + "'," + blkid + "," + bo.Created_By + ",getdate(),'Active' ");
                //    sb_qry.Append("INSERT INTO [dbo].[T21_Block_slide_Details_PhaseII] ([Block_Slide_ID],[T2_PhaseII_FormBlk_ID] ,[T50_Record_Created_By],[Created_On],[Record_Status])VALUES( ");
                //    sb_qry.Append(" '" + bo.List_Blocks[0].Block_Slide_ID + "'," + blkid + "," + bo.Created_By + ",getdate(),'Active') ");

                //    cmd.CommandText = sb_qry.ToString();
                //    cmd.ExecuteNonQuery();
                //}
                //if (bo.numberOfBlocks > 0)
                //    for (int i = 0; i < bo.numberOfBlocks; i++)
                //    {
                //        sb_qry.Append("INSERT INTO [dbo].[T21_Block_slide_Details_PhaseII] ([Block_Slide_ID],[T2_PhaseII_FormBlk_ID] ,[T50_Record_Created_By],[Created_On],[Record_Status])VALUES( ");
                //        sb_qry.Append(" '" + bo.List_Blocks[i].Block_Slide_ID + "'," + blkid + "," + bo.Created_By + ",getdate(),'Active') ");
                //        cmd.CommandText = sb_qry.ToString();
                //        cmd.ExecuteNonQuery();
                //    }


            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                Conn.Close();
            }
            return issave;
        }
        public static List<FormBLK_Blocks_BO> BlockSlideDetails(int frmblkid)
        {
            List<FormBLK_Blocks_BO> list_FormBLK_Blocks_BO = new List<FormBLK_Blocks_BO>();
            try
            {
                Conn = new SqlConnection(Co);
                sb_qry = new StringBuilder();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = Conn;
                sb_qry.Append(" SELECT  [ID],[Block_Slide_ID],[T2_PhaseII_FormBlk_ID],[Receieved_condition],[Pathology_note],");
                sb_qry.Append(" [Re_embed],[Re_process],[T50_Record_Created_By],[Created_On],[T50_Record_Modified_By],[Modified_On],");
                sb_qry.Append(" [Record_Status],[Site_Pan_No],[Dmg_Block_ID],[Dmg_Pan_No],");
                sb_qry.Append(" [isBlockReceived] FROM [T21_Block_slide_Details_PhaseII] where T2_PhaseII_FormBlk_ID=" + frmblkid + "");


                cmd.CommandText = sb_qry.ToString();
                Conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    FormBLK_Blocks_BO bo = new FormBLK_Blocks_BO();
                    bo.ID = Convert.ToInt16(dr["ID"]);
                    bo.Block_Slide_ID = dr["Block_Slide_ID"].ToString();
                    bo.T2_PhaseII_FormBlk_ID = Convert.ToInt16(dr["T2_PhaseII_FormBlk_ID"]);

                    list_FormBLK_Blocks_BO.Add(bo);
                }
                Conn.Close();

            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                Conn.Close();
            }
            return list_FormBLK_Blocks_BO;
        }




    }
}
