﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using NSABP_Model_BO_;

namespace NSABP_DAL
{
    public class FormBNK_DAL
    {
        private static string Co = ConfigurationManager.ConnectionStrings["Con"].ConnectionString;
        private static SqlConnection Conn;
        private static StringBuilder sb_qry;
        public static bool SaveBNKDetails(FormBNK_BO objFormBNKBO)
        {

            bool isSave = false;
            try
            {
                Conn = new SqlConnection(Co);
                sb_qry = new StringBuilder();
                sb_qry.Append(" insert into T5_FormBNK([FormBNK_ID],[T1_PatientID],[PersonCompletingForm_ID],[InstitutionNumber],[Dt_of_D1C1_of_neratinib_and_TDM1],");
                sb_qry.Append(" [Time_neratinib_dose_was_taken_D1C1],[Time_TDM1_dose_D1C1_started],[Dt_samples_Collected_C1D1],[Time_samples_collected_C1D1],");
                sb_qry.Append(" [Peripheral_Venious_Central_Access_Deveice_C1D1],[Time_Centrifugation_occured_C1D1],[Time_samples_was_frozen_C1D1],");
                sb_qry.Append(" [No_of_Blue_Top_microtubes_frozen_prior_to_C1D1],[No_of_Yellow_Top_microtubes_frozen_prior_to_C1D1],[T50_User_ID_CREATED_BY] ,");
                sb_qry.Append(" [FormBNK_CREATED_ON] ,[T50_User_ID_MODIFIED_BY],[FormBNK_MODIFIED_ON] ,[Record_Status]) ");
                sb_qry.Append(" values('" + objFormBNKBO.FormBNKID + "','" + objFormBNKBO.PatientID + "'," + objFormBNKBO.PersonCompletingFormID + ",");
                sb_qry.Append(" '" + objFormBNKBO.Site_Name + "','" + objFormBNKBO.Dt_of_D1C1_of_neratinib_and_TDM1 + "',  ");
                sb_qry.Append(" '" + objFormBNKBO.Time_neratinib_dose_was_taken_D1C1 + "','" + objFormBNKBO.Time_TDM1_dose_D1C1_started + "',");
                sb_qry.Append(" '" + objFormBNKBO.Dt_samples_Collected_C1D1 + "','" + objFormBNKBO.Time_samples_collected_C1D1 + "',");
                sb_qry.Append(" '" + objFormBNKBO.Peripheral_Venious_Central_Access_Deveice_C1D1 + "','" + objFormBNKBO.Time_Centrifugation_occured_C1D1 + "',");
                sb_qry.Append(" '" + objFormBNKBO.Time_samples_was_frozen_C1D1 + "'," + objFormBNKBO.No_of_Blue_Top_microtubes_frozen_prior_to_C1D1 + "");
                sb_qry.Append(" ," + objFormBNKBO.No_of_Yellow_Top_microtubes_frozen_prior_to_C1D1 + "," + objFormBNKBO.T50_User_ID_CREATED_BY + "");
                sb_qry.Append(" ,getdate()," + objFormBNKBO.T50_User_ID_MODIFIED_BY + ",getdate(),'Active')");
                SqlCommand cmd = new SqlCommand(sb_qry.ToString(), Conn);
                Conn.Open();
                cmd.ExecuteNonQuery();
                Conn.Close();
                isSave = true;
            }
            catch (Exception ex)
            {

            }
            return isSave;
        }
        public static bool UpdateUserDetails(FormBNK_BO objFormBNKBO)
        {
            bool isUpdate = false;
            try
            {
                //SqlConnection con = new SqlConnection("co");
                Conn = new SqlConnection(Co);
                sb_qry = new StringBuilder();
                sb_qry.Append("update T5_FormBNK set [Dt_of_D1C1_of_neratinib_and_TDM1]='" + objFormBNKBO.Dt_of_D1C1_of_neratinib_and_TDM1 + "',");
                sb_qry.Append(" [Time_neratinib_dose_was_taken_D1C1]='" + objFormBNKBO.Time_neratinib_dose_was_taken_D1C1 + "',[Time_TDM1_dose_D1C1_started]='" + objFormBNKBO.Time_TDM1_dose_D1C1_started + "',");
                sb_qry.Append(" [Dt_samples_Collected_C1D1]='" + objFormBNKBO.Dt_samples_Collected_C1D1 + "',[Time_samples_collected_C1D1]='" + objFormBNKBO.Time_samples_collected_C1D1 + "',");
                sb_qry.Append(" [Peripheral_Venious_Central_Access_Deveice_C1D1]='" + objFormBNKBO.Peripheral_Venious_Central_Access_Deveice_C1D1 + "' ");
                sb_qry.Append(" ,[Time_Centrifugation_occured_C1D1]='" + objFormBNKBO.Time_Centrifugation_occured_C1D1 + "',[Time_samples_was_frozen_C1D1]='" + objFormBNKBO.Time_samples_was_frozen_C1D1 + "',");
                sb_qry.Append(" [No_of_Blue_Top_microtubes_frozen_prior_to_C1D1]=" + objFormBNKBO.No_of_Blue_Top_microtubes_frozen_prior_to_C1D1 + ",");
                sb_qry.Append(" [No_of_Yellow_Top_microtubes_frozen_prior_to_C1D1]=" + objFormBNKBO.No_of_Yellow_Top_microtubes_frozen_prior_to_C1D1 + ",");
                sb_qry.Append(" [T50_User_ID_MODIFIED_BY]=" + objFormBNKBO.T50_User_ID_MODIFIED_BY + ",[FormBNK_MODIFIED_ON] = getdate() ");
                sb_qry.Append(" where Record_Status='Active' and T1_PatientID='" + objFormBNKBO.PatientID + "' and InstitutionNumber='" + objFormBNKBO.Site_Name + "'");

                SqlCommand cmd = new SqlCommand(sb_qry.ToString(), Conn);
                Conn.Open();
                cmd.ExecuteNonQuery();
                Conn.Close();
                isUpdate = true;
            }
            catch (Exception ex)
            {
            }
            return isUpdate;
        }
        public static List<FormBNK_BO> GetFormBNK_Details(string role, string site_ID)
        {

            List<FormBNK_BO> List_BNK_BO = new List<FormBNK_BO>();
            sb_qry = new StringBuilder();
            try
            {
                Conn = new SqlConnection(Co);
                sb_qry.Append("   SELECT T5.[ID],[FormBNK_ID],[T1_PatientID],T50A.FName,T50A.LName,T50A.FName+' '+T50A.LName as [Person_Completing_Form], ");
                sb_qry.Append("   [PersonCompletingForm_ID],[InstitutionNumber],T52.Site_Name,T50A.email  ");
                sb_qry.Append("   ,[Dt_of_D1C1_of_neratinib_and_TDM1],[Time_neratinib_dose_was_taken_D1C1],[Time_TDM1_dose_D1C1_started],[Dt_samples_Collected_C1D1], ");
                sb_qry.Append("    [Time_samples_collected_C1D1],[Peripheral_Venious_Central_Access_Deveice_C1D1],[Time_Centrifugation_occured_C1D1], ");
                sb_qry.Append("   [Time_samples_was_frozen_C1D1],[No_of_Blue_Top_microtubes_frozen_prior_to_C1D1],[No_of_Yellow_Top_microtubes_frozen_prior_to_C1D1]  ");
                sb_qry.Append("  ,T50.FName+' '+T50.LName as [T50_User_ID_CREATED_BY],[FormBNK_CREATED_ON] FROM [T5_FormBNK] T5  ");
                sb_qry.Append("  left join T1_User_Main T50 on (T50.User_ID=T5.PersonCompletingForm_ID and T50.Record_Status='Active') ");
                sb_qry.Append("  left join T1_User_Main T50A on (T50A.User_ID=T5.PersonCompletingForm_ID and T50A.Record_Status='Active') ");
                sb_qry.Append("  left join T8_Sites T52 on (T52.SiteID=T5.InstitutionNumber and T52.Record_Status='Active')  where T5.[Record_Status]='Active'     ");
                if (role.ToLower() == "site")
                    sb_qry.Append("  and  SiteID=   '" + site_ID + "'");
                sb_qry.Append("  order by T5.FormBNK_MODIFIED_ON desc ");
                SqlCommand cmd = new SqlCommand(sb_qry.ToString(), Conn);
                Conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    FormBNK_BO bo = new FormBNK_BO();
                    bo.ID = Convert.ToInt32(dr["ID"].ToString());
                    bo.FormBNKID = dr["FormBNK_ID"].ToString();
                    bo.PatientID = dr["T1_PatientID"].ToString();
                    bo.FormBNK_PersonCompletingForm_FName = dr["Person_Completing_Form"].ToString();
                    bo.PersonCompletingFormID = Convert.ToInt32(dr["PersonCompletingForm_ID"].ToString());
                    bo.Site_Name = dr["Site_Name"].ToString();
                    bo.SiteID = dr["InstitutionNumber"].ToString();
                    //bo.FormBNK_PersonCompletingForm_Phone = dr["Pri_Phone"].ToString();
                    bo.FormBNK_PersonCompletingForm_Email = dr["email"].ToString();
                    bo.Dt_of_D1C1_of_neratinib_and_TDM1 = Convert.ToDateTime(dr["Dt_of_D1C1_of_neratinib_and_TDM1"].ToString());
                    bo.Time_neratinib_dose_was_taken_D1C1 = dr["Time_neratinib_dose_was_taken_D1C1"].ToString();
                    bo.Time_TDM1_dose_D1C1_started = dr["Time_TDM1_dose_D1C1_started"].ToString();
                    bo.Dt_samples_Collected_C1D1 = Convert.ToDateTime(dr["Dt_samples_Collected_C1D1"].ToString());
                    bo.Time_samples_collected_C1D1 = dr["Time_samples_collected_C1D1"].ToString();
                    bo.Peripheral_Venious_Central_Access_Deveice_C1D1 = dr["Peripheral_Venious_Central_Access_Deveice_C1D1"].ToString();
                    if (bo.Peripheral_Venious_Central_Access_Deveice_C1D1.Contains("Peripheral"))
                        bo.Peripheral_venous = true;
                    else if (bo.Peripheral_Venious_Central_Access_Deveice_C1D1.Contains("Central"))
                        bo.Central_Access_Device = true;
                    bo.Time_Centrifugation_occured_C1D1 = dr["Time_Centrifugation_occured_C1D1"].ToString();
                    bo.Time_samples_was_frozen_C1D1 = dr["Time_samples_was_frozen_C1D1"].ToString();
                    bo.No_of_Blue_Top_microtubes_frozen_prior_to_C1D1 = Convert.ToInt32(dr["No_of_Blue_Top_microtubes_frozen_prior_to_C1D1"].ToString());
                    bo.No_of_Yellow_Top_microtubes_frozen_prior_to_C1D1 = Convert.ToInt32(dr["No_of_Yellow_Top_microtubes_frozen_prior_to_C1D1"].ToString());
                    List_BNK_BO.Add(bo);
                    //,[Dt_Specimen_Shipped_C1D1],[T58_Shipping_Company_ID_C1D1],[Tracking_Number_C1D1],[isSentToDMG_C1D1] 
                    //,[Dt_Specimen_Shipped_C1D8],[T58_Shipping_Company_ID_C1D8],[Tracking_Number_C1D8],[isSentToDMG_C1D8] 
                    //,[Dt_Specimen_Shipped_C2D1],[T58_Shipping_Company_ID_C2D1],[Tracking_Number_C2D1],[isSentToDMG_C2D1]
                }
                Conn.Close();

            }
            catch (Exception ex)
            {
            }

            return List_BNK_BO;
        }
    }
}
