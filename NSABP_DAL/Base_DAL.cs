﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using NSABP_Model_BO_;

namespace NSABP_DAL
{
    public class Base_DAL
    {
        private static string Co = ConfigurationManager.ConnectionStrings["Con"].ConnectionString;
        private static SqlConnection Conn;
        private static StringBuilder sb_qry = new StringBuilder();
        public static List<Sites> list_Sites()
        {
            List<Sites> list_result = new List<Sites>();
            try
            {
                Conn = new SqlConnection(Co);
                SqlCommand cmd = new SqlCommand("SELECT [ID],[SiteID],[Site_Name] FROM [dbo].[T8_Sites] where Record_Status='Active'", Conn);
                Conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Sites bo = new Sites();
                    bo.ID = Convert.ToInt16(dr["ID"]);
                    bo.SiteID = dr["SiteID"].ToString();
                    bo.Site_Name = dr["Site_Name"].ToString();
                    list_result.Add(bo);
                }
                Conn.Close();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                Conn.Close();
            }
            return list_result;
        }
        public static List<Shipping_Company> list_Shipping_Company()
        {
            List<Shipping_Company> list_result = new List<Shipping_Company>();
            try
            {
                Conn = new SqlConnection(Co);
                SqlCommand cmd = new SqlCommand("SELECT  [ID],[Company_name] FROM [T50_Shipping_Company] where Record_Status='Active'", Conn);
                Conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Shipping_Company bo = new Shipping_Company();
                    bo.ID = Convert.ToInt16(dr["ID"]);
                    bo.Company_name = dr["Company_name"].ToString();

                    list_result.Add(bo);
                }
                Conn.Close();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                Conn.Close();
            }
            return list_result;
        }
        public static List<Patients> list_Patients(string SiteID, string formname)
        {
            List<Patients> list_result = new List<Patients>();
            try
            {
                Conn = new SqlConnection(Co);
                sb_qry.Append("SELECT id, Patient_ID FROM[T9_Patients] where Record_Status = 'Active' and ScreenFailure = 0 and Institution = '" + SiteID + "'");
                if (formname == "BLK")
                    sb_qry.Append("and FormBLKCreatedDate is null");
                if (formname == "FT")
                    sb_qry.Append(" and FormFTCreatedDate is null");


                SqlCommand cmd = new SqlCommand(sb_qry.ToString(), Conn);
                Conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                sb_qry.Clear();
                while (dr.Read())
                {
                    Patients bo = new Patients();
                    bo.ID = Convert.ToInt16(dr["ID"]);
                    bo.Patient_ID = dr["Patient_ID"].ToString();
                    list_result.Add(bo);
                }
                Conn.Close();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                Conn.Close();
            }
            return list_result;
        }
        public static List<Receieved_Conditions> list_ReceivedCondition()
        {
            List<Receieved_Conditions> list_result = new List<Receieved_Conditions>();
            try
            {
                Conn = new SqlConnection(Co);
                SqlCommand cmd = new SqlCommand("SELECT [ReceievedCondition_ID],[ReceievedCondition_Name] FROM [T51_Receieved_Conditions] where Record_Status='Active'", Conn);
                Conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Receieved_Conditions bo = new Receieved_Conditions();
                    bo.ReceievedCondition_ID = Convert.ToInt16(dr["ReceievedCondition_ID"]);
                    bo.ReceievedCondition_Name = dr["ReceievedCondition_Name"].ToString();
                    list_result.Add(bo);
                }
                Conn.Close();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                Conn.Close();
            }
            return list_result;
        }

        public static List<Protocol_BO> List_Protocols(int User_ID)
        {
            List<Protocol_BO> list_result = new List<Protocol_BO>();
            try
            {
                Conn = new SqlConnection(Co);
                SqlCommand cmd = new SqlCommand("select t2.id,t2.Protocol_Code from T1_User_Main t1 join T2_Protocol_Main t2  on (t2.Id=t1.Protocol_Id and t2.Record_Status='Active') where t1.User_ID=" + User_ID + " and t1.Record_Status='Active'", Conn);
                Conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Protocol_BO bo = new Protocol_BO();
                    bo.Protocol_Id = Convert.ToInt16(dr["id"]);
                    bo.Protocol_Code = dr["Protocol_Code"].ToString();
                    list_result.Add(bo);
                }
                Conn.Close();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                Conn.Close();
            }
            return list_result;
        }
        public static List<Forms> Listforms(int ProtocolID)
        {
            List<Forms> list_result = new List<Forms>();
            try
            {
                Conn = new SqlConnection(Co);
                SqlCommand cmd = new SqlCommand("select t100.T7_FormID,t7.Form_Name,t7.URL from T7_Forms t7 join[T100_Map_T2Protocol_T7Forms] t100 on(t100.T7_FormID = t7.Form_ID and t100.Record_Status = 'Active') where t100.T2_ProtocolID = " + ProtocolID, Conn);
                Conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Forms bo = new Forms();
                    bo.ID = Convert.ToInt16(dr["T7_FormID"]);
                    bo.Form_Name = dr["Form_Name"].ToString();
                    bo.URL = dr["URL"].ToString();
                    list_result.Add(bo);
                }
                Conn.Close();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                Conn.Close();
            }
            return list_result;
        }



        //  changes For TimePoints10/10/18
        //public static List<string> GetTimePoints_Form_Protocol(int _protocolId, int _formId)
        //{
        //    List<string> listTimepoints = new List<string>();
        //    try
        //    {
        //        Conn = new SqlConnection(Co);

        //        String strQuery = "select T100_MapId,T100.T7_FormID,t102.T2_ProtocolID, TimePoint from T100_Map_T2Protocol_T7Forms t100 ";
        //        strQuery += "join T102_Map_ProtocolForm_Timepints t102 on t100.Map_ID = t102.T100_MapId where T7_FormID = " + _formId + " and t102.T2_ProtocolID=" + _protocolId + "";
        //        SqlCommand cmd = new SqlCommand(strQuery, Conn);
        //        Conn.Open();
        //        SqlDataReader dr = cmd.ExecuteReader();
        //        while (dr.Read())
        //        {
        //            listTimepoints.Add(dr["TimePoint"].ToString());
        //        }
        //        Conn.Close();

        //    }
        //    catch (Exception ex)
        //    {
        //        Conn.Close();
        //    }
        //    return listTimepoints;
        //}
        public static List<FormFT_Timepoint> GetTimePoints_Form_Protocol(int _protocolId, int _formId)
        {
            List<FormFT_Timepoint> listTimepoints = new List<FormFT_Timepoint>();
            try
            {
                Conn = new SqlConnection(Co);

                String strQuery = "select T100_MapId,T100.T7_FormID,t102.T2_ProtocolID, TimePoint,t102.Id as TimepointID from T100_Map_T2Protocol_T7Forms t100 ";
                strQuery += "join T102_Map_ProtocolForm_Timepints t102 on t100.Map_ID = t102.T100_MapId where T7_FormID = " + _formId + " and t102.T2_ProtocolID=" + _protocolId + "";
                SqlCommand cmd = new SqlCommand(strQuery, Conn);
                Conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    FormFT_Timepoint bo = new FormFT_Timepoint();
                    bo.TimePoint = dr["TimePoint"].ToString();
                    bo.Timepoint_Id = Convert.ToInt16(dr["TimepointID"]);
                    listTimepoints.Add(bo);
                }
                Conn.Close();

            }
            catch (Exception ex)
            {
                Conn.Close();
            }
            return listTimepoints;
        }
        public static List<FormFT_Timepoint> GetTimePointsDetails_ByPatient(int _protocolId, int _formId,string _patientId)
        {
            List<FormFT_Timepoint> lst_FT_tp_details = new List<FormFT_Timepoint>();
            FormFT_Timepoint bo;
            try
            {
                Conn = new SqlConnection(Co);

                //String strQuery = "select T100_MapId,T100.T7_FormID,t102.T2_ProtocolID, TimePoint from T100_Map_T2Protocol_T7Forms t100 ";
                //strQuery += "join T102_Map_ProtocolForm_Timepints t102 on t100.Map_ID = t102.T100_MapId where T7_FormID = " + _formId + " and t102.T2_ProtocolID=" + _protocolId + "";
                String strQuery = "select T100_MapId, T100.T7_FormID,t102.T2_ProtocolID, TimePoint,t10t.Id,t102.Id as TimePointId,t10t.T10_FTId,t10t.LiverTumorIssue,t10t.OtherIssue,t10t.SiteOfOtherTumorIssue,t10t.DateBiospecimenCollected,t10t.SubMergedIn10PercentFormalinVial,t10t.T10_Form_FT_ID,t10t.Record_Status from T100_Map_T2Protocol_T7Forms t100";
                strQuery += " join T102_Map_ProtocolForm_Timepints t102 on t100.Map_ID = t102.T100_MapId";
                strQuery += " left join T10_FT_Timepoint t10t on t10t.T102_Map_Id = t102.Id";
                strQuery += " left join T10_FormFT t10 on  t10.ID= t10t.T10_FTId ";
                strQuery += " where  T7_FormID = " + _formId + " and t102.T2_ProtocolID=" + _protocolId + " and t10.T9_PatientID='"+ _patientId + "'";
                SqlCommand cmd = new SqlCommand(strQuery, Conn);
                Conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    bo = new FormFT_Timepoint();
                    bo.T7_FormID = Convert.ToInt32(dr["T7_FormID"]);
                    bo.ProtocolID = Convert.ToInt32(dr["T2_ProtocolID"]);
                    if (dr["TimePoint"] != DBNull.Value)
                        bo.TimePoint = dr["TimePoint"].ToString();
                    bo.Timepoint_Id = Convert.ToInt32(dr["TimePointId"]);
                    if (dr["T10_Form_FT_ID"] != DBNull.Value)
                        bo.Form_FT_ID = dr["T10_Form_FT_ID"].ToString();
                    if (dr["LiverTumorIssue"] != DBNull.Value)
                        bo.LiverTumorIssue = (bool)(dr["LiverTumorIssue"]);
                    if (dr["OtherIssue"] != DBNull.Value)
                    {
                        bo.OtherTIssue = (bool)(dr["OtherIssue"]);                       
                    }
                    if(dr["SiteOfOtherTumorIssue"] != DBNull.Value)
                        bo.SiteOfOtherTumorTIssue = Convert.ToInt16(dr["SiteOfOtherTumorIssue"]);
                    if (dr["SubMergedIn10PercentFormalinVial"] != DBNull.Value)
                    {
                        bo.SubMergedIn10PercentFormalinVial = dr["SubMergedIn10PercentFormalinVial"].ToString();
                    }
                    
                         if (dr["DateBiospecimenCollected"] != DBNull.Value)
                    {
                        bo.DateBiospecimenCollected =(DateTime)dr["DateBiospecimenCollected"];
                    }
                    lst_FT_tp_details.Add(bo);

                }
                Conn.Close();

            }
            catch (Exception ex)
            {
                Conn.Close();
            }
            return lst_FT_tp_details;
        }
        //changes end
      
    }

}

