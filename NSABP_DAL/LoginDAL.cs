﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using NSABP_Model_BO_;

namespace NSABP_DAL
{
    public class LoginDAL
    {

        #region "Public Variables"
        /// <summary>
        /// These below created variables are public and can be used throughout the class.
        /// </summary>  
        ///  
        private static string Co = ConfigurationManager.ConnectionStrings["Con"].ConnectionString;
        static SqlConnection sqlConnection = null;
        //static SqlCommand sqlCommand = null;
        //static DataSet ds;
        static SqlDataAdapter dap;
        #endregion
        public static DataTable getuserDetails(LoginBO loginBO)
        {
            DataTable dt = new DataTable();
            try
            {
                sqlConnection = new SqlConnection(Co);
                SqlCommand sqlCommand = new SqlCommand("USP1_Authenticate_User", sqlConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@UserName", loginBO.UserName);
                sqlCommand.Parameters.AddWithValue("@Password", loginBO.Password);
                SqlDataAdapter da = new SqlDataAdapter(sqlCommand);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
            }
            return dt;

        }
        public static DataTable getForms(int user_ID)
        {
            DataTable dt = new DataTable();
            try
            {
                sqlConnection = new SqlConnection(Co);
                SqlCommand cmd = new SqlCommand("select t100.Map_ID,t100.T7_FormID from T7_Forms t7 join [T100_Map_T2Protocol_T7Forms] t100  on (t100.T7_FormID=t7.Form_ID and t100.Record_Status='Active')  join T1_User_Main t1  on (t1.Protocol_Id=t100.T2_ProtocolID and t1.Record_Status='Active')left join  T2_Protocol_Main t2 on (t2.Id=t1.Protocol_Id and t2.Record_Status='Active')where  t1.Record_Status='Active' and t1.User_ID=" + user_ID, sqlConnection);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);


            }
            catch (Exception ex)
            {
            }
            return dt;

        }
       
        //select t100.Map_ID, t100.T7_FormID from T7_Forms t7 join[T100_Map_T2Protocol_T7Forms] t100  on (t100.T7_FormID= t7.Form_ID and t100.Record_Status= 'Active')  join T1_User_Main t1 on(t1.Protocol_Id= t100.T2_ProtocolID and t1.Record_Status= 'Active')left join  T2_Protocol_Main t2 on(t2.Id= t1.Protocol_Id and t2.Record_Status= 'Active')where t1.Record_Status='Active' and t1.User_ID= 1
    }
}
